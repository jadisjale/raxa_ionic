import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { IonicApp, IonicModule, IonicErrorHandler, Platform} from 'ionic-angular';
import { MyApp } from './app.component';
import { IonicStorageModule } from '@ionic/storage';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { MenuPage } from '../pages/menu/menu';

import { StatusBar } from '@ionic-native/status-bar/';
import { SplashScreen } from '@ionic-native/splash-screen/';
import { HttpClientModule } from '@angular/common/http';
import { UserPage } from '../pages/user/user';
import { ScorersPage } from '../pages/scorers/scorers';
import { VictoriousPage } from '../pages/victorious/victorious';
import { FrequencyPage } from '../pages/frequency/frequency';
import { ClientServe } from '../tools/Client/ClientServe';
import { GoalUserPage } from '../pages/goal-user/goal-user';
import { HistoryPage } from '../pages/history/history';
import { CameraPage } from '../pages/camera/camera';
import { Camera } from '@ionic-native/camera/';
import { FileTransfer } from '@ionic-native/file-transfer/';
import { File } from '@ionic-native/file/';
import { CompareResultPage } from '../pages/compare-result/compare-result';
import { ResultPage } from '../pages/result/result';
import { SeasonPage } from '../pages/season/season';
import { NewSeasonPage } from '../pages/new-season/new-season';
import { ResultOldSeasonPage } from '../pages/result-old-season/result-old-season';
import { NewGroupPage } from '../pages/new-group/new-group';
import { ProfilePage } from '../pages/profile/profile';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { ConfigUserPage } from '../pages/config-user/config-user';
import { ImprovimentPage } from '../pages/improviment/improviment';


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    MenuPage,
    UserPage,
    ScorersPage,
    VictoriousPage,
    FrequencyPage,
    GoalUserPage,
    HistoryPage,
    CameraPage,
    CompareResultPage,
    ResultPage,
    SeasonPage,
    NewSeasonPage,
    ResultOldSeasonPage,
    NewGroupPage,
    ProfilePage,
    ConfigUserPage,
    ImprovimentPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    ClientServe,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    MenuPage,
    UserPage,
    ScorersPage,
    VictoriousPage,
    FrequencyPage,
    GoalUserPage,
    HistoryPage,
    CameraPage,
    CompareResultPage,
    ResultPage,
    SeasonPage,
    NewSeasonPage,
    ResultOldSeasonPage,
    NewGroupPage,
    ProfilePage,
    ConfigUserPage, 
    ImprovimentPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera,
    FileTransfer,
    File,
    Push
  ]
})
export class AppModule {}
