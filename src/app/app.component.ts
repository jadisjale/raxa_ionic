import { Component } from '@angular/core/';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar/';
import { SplashScreen } from '@ionic-native/splash-screen/';
import { Storage } from '@ionic/storage';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';

import { Push, PushObject, PushOptions } from '@ionic-native/push';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {

  rootPage:any;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    public storage: Storage,
    private push: Push
  ) {
    platform.ready().then(() => {
      statusBar.backgroundColorByHexString('#006400');
      statusBar.styleLightContent();
      //statusBar.styleDefault();
      splashScreen.show();
      splashScreen.hide();
      this.notification();
      this.checkPreviousAuthorization();
    });
  }

  checkPreviousAuthorization(): void { 
    this.storage.get('token').then((token: any) => {
      if(token != null) {
        this.rootPage = TabsPage;
      } else {
        this.rootPage = LoginPage;
      }
      //this.rootPage = ProfilePage;
    });
  }  

  notification() {
    const options: PushOptions = {
      android: {
        senderID: "440059627878"
      },
      ios: {
          alert: 'true',
          badge: true,
          sound: 'false'
      },
   };
   
   const pushObject: PushObject = this.push.init(options);
   
   pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));
   
   pushObject.on('registration').subscribe((registration: any) => 
      this.storage.set('token_fcm', registration.registrationId) 
   );
   
   pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }

}
