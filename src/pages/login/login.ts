import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';
import { NewGroupPage } from '../new-group/new-group';
import { Validators, FormBuilder } from '@angular/forms';
import { Push, PushObject, PushOptions } from '@ionic-native/push';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public groups;
  public valid : any = {};

  private auth: Object = {
    login: '',
    password: '',
    id_group: ''
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public toast: ToastController,
    private storage: Storage,
    public ClientServe: ClientServe,
    public formBuilder: FormBuilder,
    private push: Push
  ) {
    this.valid = this.formBuilder.group({
      id_group:['', Validators.required],
      login:['', Validators.required],
      password:['', Validators.required]
    });
  }

  ionViewDidLoad() {
    this.getGroups();
  }

  logForm() {
    try {
      this.httpClient.post(this.ClientServe.client + 'login', this.auth, this.ClientServe.httpOptions).subscribe((data: any) => {
        if (true !== data.code) {
          let toast = this.ClientServe.alertMessage(data.message);
          toast.present();
        } else {
          let toast = this.ClientServe.alertMessage('Aguarde...');
          toast.present();
          console.log(data.data);
          this.storage.set('token', data.data.token);
          this.storage.set('isAdmin', data.data.isAdmin);
          this.storage.get('token_fcm').then((token_fcm: any) => {
            this.httpClient.post(this.ClientServe.client + 'user/token/fcm', {
              'token': data.data.token,
              'token_fcm': token_fcm
            }, this.ClientServe.httpOptions).subscribe((data: any) => {
              console.log('Registrado');
            });
          });
          this.navCtrl.setRoot(TabsPage);
        }
      }); 
    } catch (e) {
      this.ClientServe.erroMessage();
    }
  }

  private getGroups() {
    this.httpClient.get(this.ClientServe.client + 'group').subscribe((data: any) => {
      console.log(data);
      this.groups = data.data;
    });
  }

  

  public addNewGroup () {
    this.navCtrl.push(NewGroupPage);
  }

}
