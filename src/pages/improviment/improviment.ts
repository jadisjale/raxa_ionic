import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';
import { NewGroupPage } from '../new-group/new-group';
import { Validators, FormBuilder } from '@angular/forms';


/**
 * Generated class for the ImprovimentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-improviment',
  templateUrl: 'improviment.html',
})
export class ImprovimentPage {

  public valid : any = {};

  private improvement: (any) = {
    token: '',
    description: '',
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public toast: ToastController,
    private storage: Storage,
    public ClientServe: ClientServe,
    public formBuilder: FormBuilder
  ) {
    this.valid = this.formBuilder.group({
      description:['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ImprovimentPage');
  }

  form() {
    this.storage.get('token').then((token: any) => {
      this.improvement.token = token;
      this.httpClient.post(this.ClientServe.client + 'improviment', this.improvement, this.ClientServe.httpOptions).subscribe((data: any) => {
          let alert = this.ClientServe.alertMessage(data.message);
          alert.present();
      });
    });
  }
}
