import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';

/**
 * Generated class for the FrequencyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-frequency',
  templateUrl: 'frequency.html',
})
export class FrequencyPage {

  public frequency: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public alert: ToastController,
    private storage: Storage,
    public ClientServe: ClientServe
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FrequencyPage');
    this.getFrequency();
  }

  private getFrequency() {

    this.storage.get('token').then((token: any) => {
      let url = 'goal/frequency/' + token;
      if (this.navParams.get('name')) {
        url = 'goal/frequency/season/' + this.navParams.get('id');
      }
      this.httpClient.get(this.ClientServe.client + url, this.ClientServe.httpOptions).subscribe((data: any) => {
        console.log(token);
        if (true !== data.code) {
          let alert = this.ClientServe.alertMessage(data.message);
          alert.present();
        } else {
          this.frequency = data.data;
        }

      });

    });

  }

  doRefresh(refresher) {
    setTimeout(() => {
      refresher.complete();
      this.getFrequency();
    }, 2000);
  }

}
