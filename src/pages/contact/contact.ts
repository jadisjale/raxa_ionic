import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';
import { HistoryPage } from '../history/history';


@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  private users: (any);

  constructor
    (
      public navCtrl: NavController,
      public navParams: NavParams,
      public httpClient: HttpClient,
      public alert: ToastController,
      private storage: Storage,
      public ClientServe: ClientServe
    ) {

  }

  ionViewDidLoad() {
    this.getUsers();
  }

  private getUsers() {
    this.storage.get('token').then((token: any) => {
      this.httpClient.get(this.ClientServe.client + 'user/all/' + token, this.ClientServe.httpOptions).subscribe((data: any) => {
        console.log(token);
        if (true !== data.code) {
          let alert = this.ClientServe.alertMessage(data.message);
          alert.present();
        } else {
          this.users = data.data;
        }
      });
    });
  }

  doRefresh(refresher) {
    setTimeout(() => {
      refresher.complete();
      this.getUsers();
    }, 2000);
  }

  history(user: any) {
    this.navCtrl.push(HistoryPage, {
      'user': user
    });
  }

}
