import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompareResultPage } from './compare-result';

@NgModule({
  declarations: [
    CompareResultPage,
  ],
  imports: [
    IonicPageModule.forChild(CompareResultPage),
  ],
})
export class CompareResultPageModule {}
