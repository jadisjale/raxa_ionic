import { Component } from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';


/**
 * Generated class for the CompareResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-compare-result',
  templateUrl: 'compare-result.html',
})
export class CompareResultPage {

  public group_user;

  public you = {
    photo: '',
    name: 'Jogador 1',
    goal: 0,
    victory: 0
  };

  public friend = {
    photo: '',
    name: 'Jogador 2',
    goal: 0,
    victory: 0,
  };

  public result : (any) = {

  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public alert: ToastController,
    public storage: Storage,
    public ClientServe: ClientServe
    ) {
      this.group_user = this.navParams.get('group_user');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompareResultPage');
  }

  ionViewDidEnter () {
    
    this.storage.get('token').then((token: any) => {
      this.httpClient.get(this.ClientServe.client + 'goal/statistics/'+token+'/'+this.group_user, this.ClientServe.httpOptions).subscribe((data: any) => {
        if (true !== data.code) {
          let alert = this.ClientServe.alertMessage(data.message);
          alert.present();
        } else {
          let request_you = data.data.you[0];
          let request_friend = data.data.friend[0];
          this.you.goal = request_you.goals;
          this.you.victory = request_you.victory;
          this.you.photo = request_you.photo;
          this.you.name = request_you.name;
          this.friend.goal = request_friend.goals;
          this.friend.victory = request_friend.victory;
          this.friend.photo = request_friend.photo;
          this.friend.name = request_friend.name;
          this.calcule();
          console.log(data);
        }
      });
    });
  }

  public calcule() {
    let gols = this.you.goal - this.friend.goal;
    let victory = this.you.victory - this.friend.victory;
    this.result = {
      'goal' : this.polo('Gol: ', gols),
      'victory' : this.polo('Vitória: ', victory)
    }
  }

  private polo (index: string, compare: number)
  {
    if (compare > 0) {
      return index + compare + " a mais"
    }

    if (compare < 0) {
      compare = compare * -1;
      return index + compare + " a menos"
    }

    if (compare === 0) {
      return index + " empate"
    }

  }

}
