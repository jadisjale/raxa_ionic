import { Component } from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';
import { Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  public valid : any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public alert: ToastController,
    public storage: Storage,
    public ClientServe: ClientServe,
    public formBuilder: FormBuilder
  ) {
    this.valid = this.formBuilder.group({
      goals:['', Validators.required],
      victory:['', Validators.required],
      draw:['', Validators.required],
      defeat:['', Validators.required],
      date:['', Validators.required],
      assistance:['', Validators.required]
    });
  }

  private goal: any = {
    victory: '0',
    draw: '0',
    defeat: '0',
    goals: '0',
    date: new Date().toISOString(),
    assistance: '0',
    token:''
  };

  ionViewDidLoad() {
    
  }

  saveGoal() {

    this.storage.get('token').then((token: any) => {

      this.goal.token = token;
      console.log(this.goal);

      this.httpClient.post(this.ClientServe.client+'goal/save', this.goal, this.ClientServe.httpOptions).subscribe((data:any) => {

        console.log(data);

        if (true === data.code) {
          console.log('limpar o formulário');
        }

        let alert = this.ClientServe.alertMessage(data.message);
        alert.present();
        
      });
    });

  }

}
