import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';
import { NewSeasonPage } from '../new-season/new-season';
import { ResultOldSeasonPage } from '../result-old-season/result-old-season';

/**
 * Generated class for the SeasonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-season',
  templateUrl: 'season.html',
})
export class SeasonPage {

  public seasons:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public alert: ToastController,
    public storage: Storage,
    public ClientServe: ClientServe
  ) {
  }

  ionViewWillEnter() {
    this.getSeasons();
  }

  private getSeasons() {
    this.storage.get('token').then((token: any) => {
      this.httpClient.get(this.ClientServe.client+'season/all/' + token, this.ClientServe.httpOptions).subscribe((data: any) => {
        if (true !== data.code) {
          let alert = this.ClientServe.alertMessage(data.message);
          alert.present();
        } else {
          this.seasons = data.data;
        }
      });
      
    });
  }

  public load(season:any) {
    if(season.isActive === false) {
      console.warn('carregar pagina da temporada');
      this.navCtrl.push(ResultOldSeasonPage, {
        'id': season.id,
        'name': season.descrpition
      })
    }
  }

  public add() {
    this.navCtrl.push(NewSeasonPage);
  }

}
