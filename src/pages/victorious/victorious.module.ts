import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VictoriousPage } from './victorious';

@NgModule({
  declarations: [
    // VictoriousPage,
  ],
  imports: [
    IonicPageModule.forChild(VictoriousPage),
  ],
})
export class VictoriousPageModule {}
