import { Component } from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';

/**
 * Generated class for the VictoriousPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-victorious',
  templateUrl: 'victorious.html',
})
export class VictoriousPage {

  private victorious:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public alert: ToastController,
    private storage: Storage,
    public ClientServe: ClientServe
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VictoriousPage');
    this.getVictorious();
  }

  private getVictorious() {

    this.storage.get('token').then((token: any) => {
      let url = 'goal/victorious/' + token;
      if (this.navParams.get('name')) {
        url = 'goal/victorious/season/' + this.navParams.get('id');
      }
      this.httpClient.get(this.ClientServe.client+url, this.ClientServe.httpOptions).subscribe((data: any) => 
      {
        if (true !== data.code) {
          let alert = this.ClientServe.alertMessage(data.message);
          alert.present();
        } else {
          this.victorious = data.data;
          console.log(this.victorious);
        }
      });
      
    });
    
  }

  doRefresh(refresher) {
    setTimeout(() => {
      refresher.complete();
      this.getVictorious();
    }, 2000);
  }

}
