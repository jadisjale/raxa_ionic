import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';
import { Validators, FormBuilder } from '@angular/forms';

/**
 * Generated class for the ConfigUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-config-user',
  templateUrl: 'config-user.html',
})
export class ConfigUserPage {

  public valid : any = {};

  private user: (any) = {
    login: '',
    password: '',
    name: '',
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public toast: ToastController,
    private storage: Storage,
    public ClientServe: ClientServe,
    public formBuilder: FormBuilder,
  ) {
    this.valid = this.formBuilder.group({
      login:['', Validators.required],
      password:['', Validators.compose([Validators.required,Validators.minLength(3)])],
      name:['', Validators.required],
    });
  }

  ionViewDidLoad() {
    this.storage.get('token').then((token: any) => {
      this.httpClient.post(this.ClientServe.client + 'user/'+token, this.user, this.ClientServe.httpOptions).subscribe((data: any) => {
        this.user.name = data.data.name;
        this.user.login = data.data.login;
      });
    });
  }

  form() {
    this.storage.get('token').then((token: any) => {
      this.httpClient.post(this.ClientServe.client + 'user/update/data/'+token, this.user, this.ClientServe.httpOptions).subscribe((data: any) => {
        let alert = this.ClientServe.alertMessage(data.message);
        alert.present();
      });
    });
  }

}
