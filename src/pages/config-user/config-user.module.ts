import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfigUserPage } from './config-user';

@NgModule({
  declarations: [
    ConfigUserPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfigUserPage),
  ],
})
export class ConfigUserPageModule {}
