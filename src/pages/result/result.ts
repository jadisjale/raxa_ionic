import { Component } from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';

/**
 * Generated class for the ResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-result',
  templateUrl: 'result.html',
})
export class ResultPage {

  public people = {
    photo: '',
    goal: 0,
    victory: 0,
    name: 'Jogador',
    game: 1
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public alert: ToastController,
    public storage: Storage,
    public ClientServe: ClientServe,
  ) {

  }

  ionViewDidLoad() {
    this.storage.get('token').then((token: any) => {
      this.httpClient.get(this.ClientServe.client + 'goal/average/' + token, this.ClientServe.httpOptions).subscribe((data: any) => {
          this.people.photo = data.data.photo;
          this.people.name = data.data.name;
          this.people.goal = data.data.goals;
          this.people.victory = data.data.victory;
          this.people.game = data.data.frequency;
      });
    });
  }

}
