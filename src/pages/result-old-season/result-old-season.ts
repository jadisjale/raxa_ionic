import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';
import { ScorersPage } from '../scorers/scorers';
import { VictoriousPage } from '../victorious/victorious';
import { FrequencyPage } from '../frequency/frequency';

/**
 * Generated class for the ResultOldSeasonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-result-old-season',
  templateUrl: 'result-old-season.html',
})
export class ResultOldSeasonPage {

  private season = {
    name: '',
    id: 0
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public alert: ToastController,
    public storage: Storage,
    public ClientServe: ClientServe
  ) 
  {
    this.season.name = this.navParams.get('name');
    this.season.id = this.navParams.get('id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultOldSeasonPage');
  }

  pageScores() {
    this.navCtrl.push(ScorersPage, {
      'name': this.season.name,
      'id'  : this.season.id
    });
  }

  pageVictorious() {
    this.navCtrl.push(VictoriousPage, {
      'name': this.season.name,
      'id'  : this.season.id
    });
  }

  pageFrequency() {
    this.navCtrl.push(FrequencyPage, {
      'name': this.season.name,
      'id'  : this.season.id
    });
  }
  
}
