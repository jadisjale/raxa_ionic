import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultOldSeasonPage } from './result-old-season';

@NgModule({
  declarations: [
    ResultOldSeasonPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultOldSeasonPage),
  ],
})
export class ResultOldSeasonPageModule {}
