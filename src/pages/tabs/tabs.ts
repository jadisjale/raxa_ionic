import { Component } from '@angular/core';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { MenuPage } from '../menu/menu';
import {NavController, NavParams, ToastController} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  public tab1Root = HomePage;
  public tab2Root = null;
  public tab3Root = ContactPage;
  public tab4Root = MenuPage;

  public admin: boolean;

  isenabled: any = 'disabled';

  public elAdmin: any = <HTMLElement><any>document.getElementsByClassName('admin');

  isAdmin() {
    this.storage.get('isAdmin').then((isAdmin: any) => {
      //se false remove os itens que estão com a classe admin
      this.admin = isAdmin;
      if (this.admin === true) {
        this.tab2Root = AboutPage;
      }
    });
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public alert: ToastController,
    public storage: Storage,
    public ClientServe: ClientServe
  ) {
  
  }

  ionViewWillEnter(){
    console.log('2 antes de entrar na pagina');
    this.isAdmin();
  }

}
