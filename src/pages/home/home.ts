import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, ActionSheetController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';
import { CameraOptions, Camera } from '@ionic-native/camera/';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/';
import { File } from '@ionic-native/file';
import { Push, PushObject, PushOptions } from '@ionic-native/push';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  cameraOn: boolean = false;
  public photo : string = 'assets/imgs/people.png';

  public fileTransfer: FileTransferObject;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public toast: ToastController,
    private storage: Storage,
    public ClientServe: ClientServe,
    private camera: Camera,
    public actionSheetController: ActionSheetController,
    private transfer: FileTransfer,
    public file: File,
  ) {

  }

  ionViewDidLoad() {
    this.getStatistic();
  }

  private statistics: any = {
    id: '0',
    name: 'Cadastre um raxa para habilitar esse menu',
    goals: '0',
    victory: '0',
    draw: '0',
    defeat: '0',
    assistance: '0',
    photo: '0'
  };

  getStatistic() {
    this.storage.get('token').then((sessao: any) => {
      this.httpClient.get(this.ClientServe.client + 'goal/statistics/' + sessao, this.ClientServe.httpOptions).subscribe((data: any) => {
        if (true !== data.code) {
          let toast = this.ClientServe.alertMessage(data.message);
          toast.present();
        } else {

          console.log(data);
          //caso haja raxa cadastrado
          if (data.data.length > 0) {

            let data_request = data.data[0];

            console.log(data_request);

            this.statistics.id = data_request.id;
            this.statistics.name = data_request.name;
            this.statistics.goals = data_request.goals;
            this.statistics.victory = data_request.victory;
            this.statistics.draw = data_request.draw;
            this.statistics.defeat = data_request.defeat;
            this.statistics.assistance = data_request.assistance;
            this.statistics.photo = data_request.photo || 'assets/imgs/people.png';
            this.photo = this.statistics.photo;
            console.warn(this.statistics);
          }

        }

      });

    });
  }

  async newPhoto() {
    const actionSheet = await this.actionSheetController.create({
      //header: 'Albums',
      buttons: [{
        text: 'Camera',
        role: 'destructive',
        icon: 'camera',
        handler: () => {
          this.getCameraPicture();
        }
      }, {
        text: 'Galeria',
        icon: 'albums',
        handler: () => {
          this.openGallery();
          console.log('Share clicked');
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel');

        }
      }]
    });
    await actionSheet.present();
  }

  private getCameraPicture() {

    this.cameraOn = true;

    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      targetWidth: 600,
      targetHeight: 600
    }
    

    this.camera.getPicture(options).then((imageData) => {
      this.photo = 'assets/imgs/people.png';
      this.cameraOn = false;
      this.upload(imageData);

    }, (err) => {
      console.log(err)
    });
  }

   private openGallery(): void {
    let cameraOptions = {
      quality: 50,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,      
      encodingType: this.camera.EncodingType.JPEG,      
      correctOrientation: true,
      targetWidth: 600,
      targetHeight: 600
    }
  
    this.camera.getPicture(cameraOptions).then((imageData) => {
      this.photo = 'assets/imgs/people.png';
      this.upload(imageData);
    }, (err) => {
    });
       
  }

  upload(file:string) {
      let options: FileUploadOptions = {
       fileKey: 'photo',
       fileName: file,
       params : {
        token: '1-1botns32jcusm49'
       },
       httpMethod: 'post',
       mimeType: 'multipart/form-data',
       chunkedMode : false
      }

    this.storage.get('token').then((token: any) => {
      let url = this.ClientServe.client + 'user/photo/' + token;
      this.fileTransfer = this.transfer.create();
      this.fileTransfer.upload(file, url, options)
      .then((data:any) => {
        this.getStatistic();
//        let alerta = this.ClientServe.alertMessage(data.message);
  //      alerta.present();
      }, (err) => {
        //alert(JSON.stringify(err));
      });
    });
  }
  
}
