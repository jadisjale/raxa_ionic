import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';
import { Validators, FormBuilder } from '@angular/forms';

/**
 * Generated class for the NewGroupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-new-group',
  templateUrl: 'new-group.html',
})
export class NewGroupPage {

  public valid : any = {};

  private groupUser: (any) = {
    group: '',
    name: '',
    login: '',
    is_admin: true,
    password: '',
    photo: '',
    descrpition: ''
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public alert: ToastController,
    public ClientServe: ClientServe,
    private storage: Storage,
    public formBuilder: FormBuilder
  ) {
    this.valid = this.formBuilder.group({ 
      descrpition:['', Validators.required],
      group:['', Validators.required],
      name:['', Validators.required],
      login:['', Validators.required],
      password:['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewGroupPage');
  }

  public form() {
    this.httpClient.post(this.ClientServe.client + 'user/new/group', this.groupUser, this.ClientServe.httpOptions).subscribe((data: any) => {
      if (true !== data.code) {
        let msg = this.ClientServe.alertMessage(data.message);
        msg.present();
      } else {

        let auth = {
          login: this.groupUser.login,
          password: this.groupUser.password,
          id_group: data.data
        };

        console.warn(auth);

        this.httpClient.post(this.ClientServe.client + 'login', auth, this.ClientServe.httpOptions).subscribe((data: any) => {
          if (true !== data.code) {
            let alert = this.ClientServe.alertMessage(data.message);
            alert.present();
          } else {
            this.storage.set('token', data.data.token);
            this.storage.set('isAdmin', data.data.isAdmin);
            this.navCtrl.setRoot(TabsPage);
          }
        });
      }
    });

  }

}
