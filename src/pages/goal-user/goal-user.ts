import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';
import { Validators, FormBuilder } from '@angular/forms'; 

/**
 * Generated class for the GoalUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-goal-user',
  templateUrl: 'goal-user.html',
})
export class GoalUserPage {

  private user: any;

  public valid : any = {};

  private newGoal: any = {
    victory: '0',
    draw: '0',
    defeat: '0',  
    goals: '0',
    date: new Date().toISOString(),
    assistance: '0',
    token: '',
  };

  constructor
    (
      public navCtrl: NavController,
      public navParams: NavParams,
      public httpClient: HttpClient,
      public alert: ToastController,
      public storage: Storage,
      public ClientServe: ClientServe,
      public formBuilder: FormBuilder
    ) {
    this.user = this.navParams.get('user');
    if (this.navParams.get('newGoal')) {
      this.newGoal = this.navParams.get('newGoal');
      this.newGoal.date = new Date(this.newGoal.date.date).toISOString();
      //this.newGoal.date.date = this.newGoal.date.toISOString();
    }

    this.valid = this.formBuilder.group({
      goal_id:[], 
      goals:['', Validators.required],
      victory:['', Validators.required],
      draw:['', Validators.required],
      defeat:['', Validators.required],
      date:['', Validators.required],
      assistance:['', Validators.required]
    });

  }

  ionViewDidLoad() {

  }

  saveGoal() {
     this.httpClient.post(this.ClientServe.client + 'goal/save/'+this.user.id, this.newGoal, this.ClientServe.httpOptions).subscribe((data: any) => {
      console.warn(data);
      if (true === data.code) {
        
      }
      let alert = this.ClientServe.alertMessage(data.message);
      alert.present();
    });
  }

}
