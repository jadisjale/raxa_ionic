import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GoalUserPage } from './goal-user';

@NgModule({
  declarations: [
    // GoalUserPage,
  ],
  imports: [
    IonicPageModule.forChild(GoalUserPage),
  ],
})
export class GoalUserPageModule {}
