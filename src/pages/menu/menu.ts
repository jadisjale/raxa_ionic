import { Component } from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { UserPage } from '../user/user';
import { ScorersPage } from '../scorers/scorers';
import { VictoriousPage } from '../victorious/victorious';
import { FrequencyPage } from '../frequency/frequency';
import { LoginPage } from '../login/login';
import {App} from 'ionic-angular';
import { ClientServe } from '../../tools/Client/ClientServe';
import { HistoryPage } from '../history/history';
import { ResultPage } from '../result/result';
import { SeasonPage } from '../season/season';
import { ProfilePage } from '../profile/profile';
import { ConfigUserPage } from '../config-user/config-user';
import { ImprovimentPage } from '../improviment/improviment';

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  isenabled:any = 'disabled';

  public admin: boolean;

  public elAdmin:any = <HTMLElement><any>document.getElementsByClassName('admin');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public alert: ToastController,
    public storage: Storage,
    private app:App,
    public ClientServe: ClientServe
  ) {
  
  }

  ionViewDidLoad() {

  }

  pageHistory() {
    this.navCtrl.push(HistoryPage);
  }

  pageSeason() {
    this.navCtrl.push(SeasonPage);
  }

  pageNewUser() {
    this.navCtrl.push(UserPage);
  }

  pageScores() {
    this.navCtrl.push(ScorersPage);
  }

  pageVictorious() {
    this.navCtrl.push(VictoriousPage);
  }

  pageFrequency() {
    this.navCtrl.push(FrequencyPage);
  }

  profilePage() {
    this.navCtrl.push(ProfilePage);
  }

  isAdmin () {
    this.storage.get('isAdmin').then((isAdmin: any) => {
      //se false remove os itens que estão com a classe admin
      this.admin = isAdmin;
      if (isAdmin === false) {
        var i;
        for (i = 0; i < this.elAdmin.length; i++) { 
          let el = this.elAdmin[i];
          el.classList.add('disabled');
        }
      }
    });
  }

  ionViewWillEnter(){
    console.log('2 antes de entrar na pagina');
    this.isAdmin();
  }

  logout() {
    this.storage.get('token').then((token: any) => {
      this.httpClient.get(this.ClientServe.client+'login/logout/'+token, this.ClientServe.httpOptions).subscribe((data:any) => {
        if (true !== data.code) {
          let alert = this.ClientServe.alertMessage(data.message);
          alert.present();
        } else {
          this.storage.clear();
          this.app.getRootNav().setRoot(LoginPage);
        } 
      });
    });
  }

  result() {
    this.navCtrl.push(ResultPage);
  }

  improvement() {
    this.navCtrl.push(ImprovimentPage);
  }

  pageConfigUser() {
    this.navCtrl.push(ConfigUserPage);
  }

}
