import { Component } from '@angular/core';
import {NavController, NavParams, ToastController, ActionSheetController} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';
import { GoalUserPage } from '../goal-user/goal-user';
import { CompareResultPage } from '../compare-result/compare-result';

/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  private history: any;
  private user: any;
  private admin: boolean;
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public alert: ToastController,
    public storage: Storage,
    public ClientServe: ClientServe,
    public actionSheetController: ActionSheetController
  ) {
    this.user = this.navParams.get('user');
    console.log(this.user);
  }

  ionViewWillEnter () {
    if (this.user) {
      this.isAdmin();    
      this.getHistoryByIdGroupUser();
    } else {
      this.getHistory();
    }
  }

  ionViewDidLoad() {    
    if (this.user) {
      this.isAdmin();    
      this.getHistoryByIdGroupUser();
    } else {
      this.getHistory();
    }
  }

  isAdmin() {
    this.storage.get('isAdmin').then((isAdmin: any) => {
      this.admin = isAdmin;
    });
  }

  private getHistory() {
    this.storage.get('token').then((token: any) => {
      console.log(this.ClientServe.client + 'goal/history/' + token);
      this.httpClient.get(this.ClientServe.client + 'goal/history/' + token, this.ClientServe.httpOptions).subscribe((data: any) => {
        if (true !== data.code) {
          let alert = this.ClientServe.alertMessage(data.message);
          alert.present();
        } else {
          this.history = data.data;
          console.log(this.history);
        }
      });
    });
  }

  private getHistoryByIdGroupUser() {
      this.httpClient.get(this.ClientServe.client + 'goal/history/group/user/' + this.user.id, this.ClientServe.httpOptions).subscribe((data: any) => {
        if (true !== data.code) {
          let alert = this.ClientServe.alertMessage(data.message);
          alert.present();
        } else {
          this.history = data.data;
        }
      });
  }

  doRefresh(refresher) {
    setTimeout(() => {
      refresher.complete();
      if (this.user) {
        this.getHistoryByIdGroupUser();
      } else {
        this.getHistory();
      }
    }, 2000);
  }

  addRaxa() {
    this.navCtrl.push(GoalUserPage, {
      'user': this.user
    });
  }

  async choice(goal:any) {
    if (this.admin) {
      const actionSheet = await this.actionSheetController.create({
        //header: 'Albums',
        buttons: [{
          text: 'Editar',
          role: 'destructive',
          icon: 'football',
          handler: () => {
            this.editRaxa(goal);
          }
        }, {
          text: 'Excluir',
          icon: 'trash',
          handler: () => {
            this.removeRaxa(goal);
          }
        }, {
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel',
          handler: () => {
  
          }
        }]
      });
      await actionSheet.present();
    }
  }

  public removeRaxa(goal: any) {
    this.storage.get('token').then((token: any) => {
      this.httpClient.post(this.ClientServe.client + 'goal/delete/'+token+"?XDEBUG_SESSION_START=PHPSTORM", {'id': goal.goal_id}, this.ClientServe.httpOptions).subscribe((data: any) => {
        if (true === data.code) {
          if (this.user) {
            this.getHistoryByIdGroupUser();
          } else {
            this.getHistory();
          }
        }
        let alert = this.ClientServe.alertMessage(data.message);
        alert.present();
      });
    });
  }

  public editRaxa(newGoal:any) {
    this.navCtrl.push(GoalUserPage, {
      'user': this.user,
      'newGoal': newGoal
    }); 
  }

  public compare() {
    this.navCtrl.push(CompareResultPage, {
      'group_user': this.user.group_user 
    }); 
  }

}
