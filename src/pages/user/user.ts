import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';
import { Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {

  public valid : any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public alert: ToastController,
    public storage: Storage,
    public ClientServe: ClientServe,
    public formBuilder: FormBuilder
  ) {

    this.valid = this.formBuilder.group({ 
      name:['', Validators.required],
      login:['', Validators.required],
      is_admin:['', Validators.required],
      password:['', Validators.required]
    });

  }

  ionViewDidLoad() {

  }

  private user: any = {
    login: '',
    is_admin: 0,
    password: '',
    photo: '',
    name: '',
    token: ''
  };

  saveGoal() {
    this.storage.get('token').then((token: any) => {

      this.user.token = token;

      console.log(this.user);

      this.httpClient.post(this.ClientServe.client + 'user', this.user, this.ClientServe.httpOptions).subscribe((data: any) => {

        console.log(data);

        if (true === data.code) {
          console.log('limpar o formulário');
        }

        let alert = this.ClientServe.alertMessage(data.message);
        alert.present();

      });
    });
  }

}
