import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';

/**
 * Generated class for the NewSeasonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-new-season',
  templateUrl: 'new-season.html',
})
export class NewSeasonPage {

  private newSeason: any = {
    descrpition: 'Temporada ',
    token: '',
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public alert: ToastController,
    public storage: Storage,
    public ClientServe: ClientServe
  ) {
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad NewSeasonPage');
  }

  saveNewSeason() {
    this.storage.get('token').then((token: any) => {
      this.newSeason.token = token;
      this.httpClient.post(this.ClientServe.client + 'season/save', this.newSeason, this.ClientServe.httpOptions).subscribe((data: any) => {
        let alert = this.ClientServe.alertMessage(data.message);
        alert.present();
        this.navCtrl.pop();
      });
    });
 }

}
