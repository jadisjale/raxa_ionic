import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewSeasonPage } from './new-season';

@NgModule({
  declarations: [
    NewSeasonPage,
  ],
  imports: [
    IonicPageModule.forChild(NewSeasonPage),
  ],
})
export class NewSeasonPageModule {}
