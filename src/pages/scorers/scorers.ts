import { Component } from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';

/**
 * Generated class for the ScorersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-scorers',
  templateUrl: 'scorers.html',
})
export class ScorersPage {

  public season = {
    name: '',
    id: 0
  }
  public scores:(any);

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public alert: ToastController,
    private storage: Storage,
    public ClientServe: ClientServe
  ) {
  }

  ionViewDidLoad() {
    this.getScores();
  }

  private getScores() {

    this.storage.get('token').then((token: any) => {
      let url = 'goal/goals/' + token;

      if (this.navParams.get('name')) {
          url = 'goal/goals/season/' + this.navParams.get('id');
          console.warn(this.navParams.get('id'));
      }

      console.warn(url);
      
      this.httpClient.get(this.ClientServe.client+url, this.ClientServe.httpOptions).subscribe((data: any) => 
      {
        console.log(token);
        if (true !== data.code) {
          let alert = this.ClientServe.alertMessage(data.message);
          alert.present();
        } else {
          this.scores = data.data;
        }

      });
      
    });

  }

  doRefresh(refresher) {
    setTimeout(() => {
      refresher.complete();
      this.getScores();
    }, 2000);
  }

}
