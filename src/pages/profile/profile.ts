import { Component } from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { ClientServe } from '../../tools/Client/ClientServe';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  private profile: (any) = {
    token: '',
    people: '',
    password: ''
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpClient: HttpClient,
    public alert: ToastController,
    public storage: Storage,
    public ClientServe: ClientServe
  ) {
  }


  ionViewDidLoad() {
    this.storage.get('token').then((sessao: any) => {
      this.httpClient.get(this.ClientServe.client + 'goal/statistics/' + sessao, this.ClientServe.httpOptions).subscribe((data: any) => {
        console.warn(data);
        if (true !== data.code) {
          let alert = this.ClientServe.alertMessage(data.message);
          alert.present();
        } else {
          if (data.data.length > 0) {
            let data_request = data.data[0];
            this.profile.people = data_request.name;
            this.profile.token = sessao;
          }
        }
      });
    });
    console.warn(this.profile);
  }

  save() {
    this.storage.get('token').then((token: any) => {
      this.httpClient.post(this.ClientServe.client+'user/profile/' , this.ClientServe.httpOptions).subscribe((data:any) => {
        let alert = this.ClientServe.alertMessage(data.message);
        alert.present();
      });
    });
  }

}
