import { ToastController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { NgModule } from '@angular/core';

@NgModule({})
export class ClientServe {

    constructor(
        public httpClient: HttpClient,
        public alert: ToastController,
        public storage: Storage
    ) {

    }

    //public client: string = 'http://raxa.dev.online/';

    public client: string = 'http://raxa.online/';

    //public client: string = 'http://192.168.10.18/';

    public httpOptions: any = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
        })
    };

    public alertMessage (msg:string = 'Carregando, aguarde') {
        return this.alert.create({
            message: msg,
            duration: 3000,
            position: 'top'
        });
    }

    public erroMessage (msg:string = 'Ei 10 ano, vai com calma, parece que ta sem net') {
        return this.alert.create({
            message: msg,
            duration: 4000,
            position: 'top'
        }).present();
    }

}
